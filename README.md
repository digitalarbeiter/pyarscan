# pyarscan

Scan Python Diffs for Architecture Decision Records.

(!) Work in Progress - doesn't work yet! (!)

Check if there were any architectural changes in this commit, and
if there were, create an ADR documentation stub for it and decline
the commit (to give the author a chance to comment the ADR).

Architectural changes may be indicated by:
 * new (or deleted) imports
 * requirements
 * click.options
 * ...

More on Architectural Decision Records:
 * https://adr.github.io/
 * https://pypi.org/project/adr-tools-python/


## Git precommit Hook

    #!/bin/bash
    if git rev-parse --verify HEAD >/dev/null 2>&1
    then
        against=HEAD
    else
        # initial commit: diff against an empty tree object
        against=$(git hash-object -t tree /dev/null)
    fi
    exec 1>&2
    git diff --cached -z $against > .naked_diff
    adr-init >&/dev/null
    arscan .naked_diff && rm .naked_diff && exit 0
    #git add doc/adr/
    exit 1
